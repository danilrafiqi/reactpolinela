import React, { Component } from 'react';

class IniParent extends Component {
  render() {
    return (
      <div>
        <IniChild judul="Laptop" satu="Acer" dua="Asus" tiga="Lenovo" />
        <IniChild judul="Motor" satu="Suzuki" dua="Yamaha" tiga="Honda" />
        <IniChild judul="Mobil" satu="Honda" dua="Toyota" tiga="Mitsubishi" />
        <IniChild judul="Smartphone" satu="Vivo" dua="Oppo" tiga="Samsung" />
      </div>
    );
  }
}

class IniChild extends React.Component {
  render() {
    return (
      <div>
        <h2>{this.props.judul}</h2>
        <div>{this.props.satu}</div>
        <div>{this.props.dua}</div>
        <div>{this.props.tiga}</div>
      </div>
    );
  }
}

export default IniParent;
