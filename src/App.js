import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';

import MainRouter from './components/MainRouter';

class App extends Component {
  render() {
    return <MainRouter />;
  }
}

export default App;
