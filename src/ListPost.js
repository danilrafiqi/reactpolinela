import React, { Component } from 'react';
import Axios from 'axios';

class ListPost extends Component {
  state = {
    blog: []
  };

  all = () => {
    Axios.get('https://jsonplaceholder.typicode.com/posts').then(res => {
      this.setState({
        blog: res.data
      });
    });
  };

  componentDidMount() {
    this.all();
  }

  render() {
    return (
      <div>
        {this.state.blog.map((datas, index) => {
          return (
            <div key={index}>
              <h4>{datas.title}</h4>
            </div>
          );
        })}
      </div>
    );
  }
}

export default ListPost;
