import React from 'react';

class IniForm extends React.Component {
  state = {
    judul: 'ini judulnya'
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <div>
        <h3>{this.state.judul}</h3>
        <input
          name="judul"
          onChange={this.handleChange}
          type="text"
          placeholder="isikan sesuatu"
        />
      </div>
    );
  }
}

export default IniForm;
