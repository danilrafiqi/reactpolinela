import React from 'react';
import Component2 from './Component2';

class Component1 extends React.Component {
  state = {
    judul: 'halo dunia'
  };
  render() {
    return (
      <div>
        <Component2 judul={this.state.judul} />
      </div>
    );
  }
}

export default Component1;
