import React from 'react';
import IniChild from './IniChild';

class IniParent extends React.Component {
  render() {
    return <IniChild judul="Halo" satu="Halo 1" dua="Halo 2" tiga="Halo 3" />;
  }
}

export default IniParent;
