import React from 'react';

class Daftar extends React.Component {
  render() {
    return (
      <div>
        <h2>{this.props.judul}</h2>
        <div>{this.props.satu}</div>
        <div>{this.props.dua}</div>
        <div>{this.props.tiga}</div>
      </div>
    );
  }
}

export default Daftar;
