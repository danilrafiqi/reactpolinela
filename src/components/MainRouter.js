import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Contact from './Contact';
import About from './About';
import Home from './Home';

export default class MainRouter extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/contact" component={Contact} />
        <Route path="/about" component={About} />
      </Switch>
    );
  }
}
