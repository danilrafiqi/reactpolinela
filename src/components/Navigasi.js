import React, { Component } from 'react';
// import logo from './logo.svg';
import { Link } from 'react-router-dom';

class Navigasi extends Component {
  render() {
    return (
      <div className="">
        <Link to="/home">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/contact">Contact</Link>
      </div>
    );
  }
}

export default Navigasi;
