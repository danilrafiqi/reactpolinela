import React from 'react';
import Component3 from './Component3';

class Component2 extends React.Component {
  state = {
    judul: 'halo dunia'
  };
  render() {
    return (
      <div>
        <Component3 kirim={this.props.judul} />
      </div>
    );
  }
}

export default Component2;
