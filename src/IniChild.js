import React from 'react';

class IniChild extends React.Component {
  render() {
    return (
      <div>
        <h1>{this.props.judul}</h1>
        <div>{this.props.satu}</div>
        <div>{this.props.dua}</div>
        <div>{this.props.tiga}</div>
      </div>
    );
  }
}

export default IniChild;
