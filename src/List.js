import React from 'react';

class List extends React.Component {
  render() {
    return (
      <div>
        <h2>{this.props.judul}</h2>
        <div>{this.props.barang1}</div>
        <div>{this.props.barang2}</div>
        <div>{this.props.barang3}</div>
      </div>
    );
  }
}

export default List;
